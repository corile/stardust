const express = require("express")
const bodyParser = require("body-parser")
const fs = require("fs")

const app = express()
const port = process.env.PORT || 80;
const router = express.Router()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

router.get('/', (req, res) => {
  res.send("This will one day be a proper front-end site. Just you wait.")
})

router.route('/ids').get((req,res) => {
  fs.readFile("./ID.json", "utf-8", (err, data) => {
    if(err) throw err
    res.json(JSON.parse(data))
  })
})

router.route("/ids/nick/:nickname").get((req,res) => {
  let nickname = req.params.nickname
  fs.readFile("./ID.json", "utf-8", (err, data) => {
    if(err) throw err
    data = JSON.parse(data)
    res.json(data[nickname])
  })
})

app.use('/stardust', router)

app.listen(port)
